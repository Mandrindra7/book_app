import React from 'react'

import { Routes, Route} from 'react-router-dom'

import Home from './pages/Home'
import AddBooks from './pages/AddBooks'
import EditBook from './pages/EditBooks'
import DeleteBook from './pages/DeleteBooks'
import BookDetails from './pages/BookDetails'

const App = () => {
  return (
    <Routes>
      <Route path='/' element={<Home />}/>
      <Route path='/books/:id?' element={<AddBooks/>}/>
      {/* <Route path='/books/edit/:id' element={<EditBook/>}/> */}
      <Route path='/books/delete/:id' element={<DeleteBook/>}/>
      <Route path='/books/details/:id' element={<BookDetails/>}/>
    </Routes>
  )
}

export default App