import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import BackButton from "../components/BackButton";
import { createBook, getBook } from "../service/Books";
import Spinner from "../components/Spinner";

const AddBooks = () => {
  const [form, setForm] = useState({});
  const [loading, setLoading] = useState(false);
  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    id && getBookDetail(id);
  }, [id]);

  const getBookDetail = async (bookId) => {
    setLoading(true);
    const res = await getBook(bookId);
    res && setForm({ ...res.data });
    setLoading(false);
  };

  const handleChange = (e) => {
    form[e.target.name] = e.target.value;
    setForm({ ...form });
  };

  const saveBook = async () => {
    setLoading(true);
    const res =  id
      ? await updateBook(id, { ...form })
      : await createBook({ ...form });
    res && setLoading(false);
    navigate("/");
  };

  const setData = (name) => {
    return {
      name,
      value: form[name] || "",
      onChange: handleChange,
    };
  };
  return (
    <div className="p-4 w-fit mx-auto">
      <BackButton />
      <h1 className="text-3xl my-4">{ id ? 'Add Book' :' Edit Book' }</h1>
      {loading && <Spinner />}
      <div className="flex flex-col border-2 border-green-500 rounded-xl w-[600px] p-4">
        <div className="my-4">
          <label className="text-xl mr-4 text-green-500">Title</label>
          <input
            type="text"
            className="border-2 border-gray-500 px-4 py-2 w-full rounded-xl"
            {...setData("title")}
          />
        </div>
        <div className="my-4">
          <label className="text-xl mr-4 text-green-500">Author</label>
          <input
            type="text"
            className="border-2 border-gray-500 px-4 py-2 w-full rounded-xl"
            {...setData("author")}
          />
        </div>
        <div className="my-4">
          <label className="text-xl mr-4 text-green-500">Publish Year</label>
          <input
            type="number"
            className="border-2 border-gray-500 px-4 py-2 w-full rounded-xl"
            {...setData("publishYear")}
          />
        </div>
        <button
          className="my-4 p-2 bg-green-500 rounded-xl text-xl"
          onClick={saveBook}
        >
          Save
        </button>
      </div>
    </div>
  );
};

export default AddBooks;
