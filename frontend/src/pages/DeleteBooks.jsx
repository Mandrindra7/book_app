import { useState } from "react";

import { useNavigate, useParams } from "react-router-dom";

import { deleteBook } from "../service/Books";

import BackButton from "../components/BackButton";
import Spinner from "../components/Spinner";

const DeleteBooks = () => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const { id } = useParams();

  const removeBook = async () => {
    setLoading(true);
    const res = await deleteBook(id);
    res && setLoading(false);
    navigate("/");
  };
  return (
    <div className="p-4">
      <BackButton />
      <h1 className="text-3xl my-4">Delete Book</h1>
      {loading && <Spinner />}
      <div className="flex flex-col items-center border-2 border-green-500 rounded-xl w-[600px] p-8 mx-auto">
        <h3 className="text-2xl">Are you sure you want to delete this book?</h3>
        <div className="flex justify-around">
          <button
            className="p-4 bg-white text-gray-500 m-8 rounded-md"
            onClick={() => navigate("/")}
          >
            Cancel
          </button>
          <button
            className="p-4 bg-red-500 text-white m-8 rounded-md"
            onClick={removeBook}
          >
            Yes,delete it!
          </button>
        </div>
      </div>
    </div>
  );
};

export default DeleteBooks;
