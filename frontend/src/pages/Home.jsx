import { useEffect, useState } from "react";

import { Link } from "react-router-dom";

import { MdOutlineAddBox } from "react-icons/md";

import Spinner from "../components/Spinner";
import { getBooks } from "../service/Books";
import BookTable from "../components/BookTable";
import BookCard from "../components/BookCardList";

const Home = () => {
  const [books, setBooks] = useState([]);
  const [showType, setShowType] = useState("table");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getAllBooks();
  }, []);

  const getAllBooks = async () => {
    setLoading(true);
    const res = await getBooks();
    res && setBooks(res.data.data);
    setLoading(false);
  };

  return (
    <div className="p-4">
      <div className="flex justify-center items-center gap-x-4">
        <button
          className="bg-gray-300 hover:bg-green-600 px-4 py-2 rounded-lg"
          onClick={() => setShowType("table")}
        >
          Table
        </button>
        <button
          className="bg-gray-300 hover:bg-green-600 px-4 py-2 rounded-lg"
          onClick={() => setShowType("card")}
        >
          Card
        </button>
      </div>
      <div className="flex justify-center items-center gap-x-4">
        <h1 className="text-4xl my-8">Books List</h1>
        <Link to="/books/">
          <MdOutlineAddBox className="text-green-800 text-4xl" />
        </Link>
      </div>
      {loading ? (
        <Spinner />
      ) : showType === "table" ? (
        <BookTable books={books} />
      ) : (
        <BookCard books={books} />
      )}
    </div>
  );
};

export default Home;
