import { useEffect, useState } from "react";

import { useParams } from "react-router-dom";
import BackButton from "../components/BackButton";
import Spinner from "../components/Spinner";
import { getBook } from "../service/Books";

const BookDetails = () => {
  const [book, setBook] = useState();
  const [loading, setLoading] = useState(false);
  const { id } = useParams();

  useEffect(() => {
    id && getBookDetail(id);
  }, [id]);

  const getBookDetail = async (bookId) => {
    setLoading(true);
    const res = await getBook(bookId);
    res && setBook(res.data);
    setLoading(false);
  };
  return (
    <div className="p-4">
      <BackButton />
      <h1 className="text-3xl my-4">Book Details</h1>
      {loading  ? (
        <Spinner />
      ) : book ? (
        <div className="flex flex-col border-2 border-green-500 rounded-xl w-fit p-4">
          <div className="my-4">
            <span className="text-xl mr-4 text-green-500">Id:</span>
            <span>{book._id}</span>
          </div>
          <div className="my-4">
            <span className="text-xl mr-4 text-green-500">Title:</span>
            <span>{book.title}</span>
          </div>
          <div className="my-4">
            <span className="text-xl mr-4 text-green-500">Author:</span>
            <span>{book.author}</span>
          </div>
          <div className="my-4">
            <span className="text-xl mr-4 text-green-500">Publish Year</span>
            <span>{book.publishYear}</span>
          </div>
          <div className="my-4">
            <span className="text-xl mr-4 text-green-500">Created date</span>
            <span>{new Date(book.createdAt).toString()}</span>
          </div>
          <div className="my-4">
            <span className="text-xl mr-4 text-green-500">
              Last updated date
            </span>
            <span>{new Date(book.updatedAt).toString()}</span>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default BookDetails;
