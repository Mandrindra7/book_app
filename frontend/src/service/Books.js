import axios from "axios";

const URI = "http://localhost:5000/books";

export const getBooks = async () => {
  return await axios.get(`${URI}`);
};

export const getBook = async (id) => {
  return await axios.get(`${URI}/${id}`);
};
export const createBook = async (data) => {
  return await axios.post(`${URI}`, data);
};
export const updateBook = async (id, data) => {
  return await axios.put(`${URI}/${id}`, data);
};
export const deleteBook = async (id) => {
  console.log(id);
  return await axios.delete(`${URI}/${id}`);
};
