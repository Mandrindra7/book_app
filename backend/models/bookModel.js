import { Schema, model } from "mongoose";

const bookSchema = Schema(
  {
    title: {
      type: String,
      required: true,
    },
    author: {
      type: String,
      required: true,
    },
    publishYear: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

export const Book = model('Cat',bookSchema);
