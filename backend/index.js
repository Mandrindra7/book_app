import express from "express";

import mongoose from "mongoose";
import cors from 'cors'

import { PORT, mongoURL } from "./config.js";
import books from "./routes/books.js"

const app = express();

app.use(express.json());

app.use(cors('*'))

app.use('/books', books)

mongoose
  .connect(mongoURL)
  .then(() => {
    console.log("App connected to database");
    app.listen(PORT, () => {
      console.log(`App is listening to port : ${PORT}`);
    });
  })
  .catch((error) => console.log(error));
